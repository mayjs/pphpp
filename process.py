import sys
import re

expressionPattern = re.compile('<%=py(.*?)py%>\n?',re.DOTALL) #re.DOTALL means that . also includes new line characters.
executePattern = re.compile('<%py(.*?)py%>\n?',re.DOTALL)

outstr = ""
use_outstr = False

try: 
	from _macros import * # Import functions and classes from _macros.py if it is available
except ImportError:
	pass		

def processFile(fin,fout,files=True):
	global outstr
	global use_outstr
	content = fin.read()
	while True:
		e = expressionPattern.search(content)
		if e is None:
			c = executePattern.search(content)
			if c is None:
				break
			use_outstr = True
			outstr = ""
			exec("global outstr\n" + c.group(1).strip()) # prepend 'global outstr' to the script so that it can use the outstr variable.
			use_outstr = False
			content = content[:c.start()] + outstr + content[c.end():]
		else:
			content = content[:e.start()] + str(eval(e.group(1).strip())) + content[e.end():]
	if content[-1] != '\n':
		content += '\n'			# Ensure that the resulting code ends with a newline
	if files:
		fout.seek(0)
	fout.write(content)
	if files:
		fout.truncate()
	fout.close()

####### PREDEFINED MACRO FUNCTIONS #######

# These functions can be called from within the preprocessed file
def includeFile(filename,overrideMode=False):
	global outstr
	with open(filename) as f:
		if use_outstr and not overrideMode: # If use_outstr is set, write the file to the outstr object. This can be overridden by setting overrideMode if needed.
			outstr += f.read()
		else: 
			return f.read()


####### MAIN ########

if __name__ == "__main__":
	if len(sys.argv) == 1: # If no file is given, read from stdin and write to stdout
		processFile(sys.stdin,sys.stdout,False)
	else:
		for x in sys.argv[1:]:
			fileObj = open(x,"r+")
			processFile(fileObj,fileObj)
