# PPHPP
PPHPP uses python code snippets to preprocess your files.

## Using PPHPP 
To run PPHPP just use 'python process.py' or 'python process.py <FILENAME>'.
If you use the variant without additional parameters, the program will read all
lines available from STDIN and write the processed lines to STDOUT.
If you pass one or multiple filenames, PPHPP will process each given file and
write the result to the same file.

## Syntax in files
PPHPP will only alter code that is marked. To mark code snippets surround them
with `<%py py%>` or `<%=py py%>`. The difference between the two is that in the 
first case, a string object called `outstr` will be available that you can pass
your output to.
If you use the `<%=py` the inner statement will be processed and the result will
be written to the file.

## Example
Suppose you have a file `test.xyz`:
```
<%=py 4+2 py%>
```
After you run `python process.py test.xyz` your file will look like this:
```
6
```

## Builtin functions
Some functions are builtin to the process.py script so that you can use them in
your script.
Currently the only such function is includeFile, which loads the file with the
given name and outputs it.